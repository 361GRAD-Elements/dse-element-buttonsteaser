<?php

/**
 * 361GRAD Element Buttons Teaser
 *
 * @package   dse-elements-buttonsteaser
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

namespace Dse\ElementsBundle\ElementButtonsTeaser;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Configures the 361GRAD Element Buttons Teaser
 */
class DseElementButtonsTeaser extends Bundle
{
}
