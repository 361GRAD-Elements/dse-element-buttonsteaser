<?php

/**
 * 361GRAD Element Buttons Teaser
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietarybuttonsteaser_legend
 */

$GLOBALS['TL_LANG']['tl_content']['buttonsteaser_legend']   = 'Teaser settings';
$GLOBALS['TL_LANG']['CTE']['dse_elements'] = 'DSE-Elements';
$GLOBALS['TL_LANG']['CTE']['dse_buttonsteaser'] = ['Buttons Teaser', '1-4 Buttons Teaser'];

$GLOBALS['TL_LANG']['tl_content']['dse_bgcolor']  =
    ['Background Color', 'Here you can add a Background Color for whole elements.'];
$GLOBALS['TL_LANG']['tl_content']['dse_align']  =
    ['Buttons align', 'Here you can define an Align for the buttons.'];

$GLOBALS['TL_LANG']['tl_content']['buttonone_legend']   = 'First Button settings';
$GLOBALS['TL_LANG']['tl_content']['dse_buttonOneHref']  =
    ['First Button Link', 'Here you can add a Link for First Button.'];
$GLOBALS['TL_LANG']['tl_content']['dse_buttonOneTarget']  =
    ['First Button Target', 'Here you can add a Target (same/new page) for First Button.'];
$GLOBALS['TL_LANG']['tl_content']['dse_buttonOneText']  =
    ['First Button Text', 'Here you can add a Text for First Button.'];
$GLOBALS['TL_LANG']['tl_content']['dse_buttonOneColor']  =
    ['Background Color', 'Here you can add a Background Color for this button (use this as standalone option).'];

$GLOBALS['TL_LANG']['tl_content']['buttontwo_legend']   = 'Second Button settings';
$GLOBALS['TL_LANG']['tl_content']['dse_buttonTwoHref']  =
    ['Second Button Link', 'Here you can add a Link for Second Button.'];
$GLOBALS['TL_LANG']['tl_content']['dse_buttonTwoTarget']  =
    ['Second Button Target', 'Here you can add a Target (same/new page) for Second Button.'];
$GLOBALS['TL_LANG']['tl_content']['dse_buttonTwoText']  =
    ['Second Button Text', 'Here you can add a Text for Second Button.'];
$GLOBALS['TL_LANG']['tl_content']['dse_buttonTwoColor']  =
    ['Background Color', 'Here you can add a Background Color for this button (use this as standalone option).'];

$GLOBALS['TL_LANG']['tl_content']['buttonthree_legend']   = 'Third Button settings';
$GLOBALS['TL_LANG']['tl_content']['dse_buttonThreeHref']  =
    ['Third Button Link', 'Here you can add a Link for Third Button.'];
$GLOBALS['TL_LANG']['tl_content']['dse_buttonThreeTarget']  =
    ['Third Button Target', 'Here you can add a Target (same/new page) for Third Button.'];
$GLOBALS['TL_LANG']['tl_content']['dse_buttonThreeText']  =
    ['Third Button Text', 'Here you can add a Text for Third Button.'];
$GLOBALS['TL_LANG']['tl_content']['dse_buttonThreeColor']  =
    ['Background Color', 'Here you can add a Background Color for this button (use this as standalone option).'];

$GLOBALS['TL_LANG']['tl_content']['buttonfour_legend']   = 'Fourth Button settings';
$GLOBALS['TL_LANG']['tl_content']['dse_buttonFourHref']  =
    ['Fourth Button Link', 'Here you can add a Link for Fourth Button.'];
$GLOBALS['TL_LANG']['tl_content']['dse_buttonFourTarget']  =
    ['Fourth Button Target', 'Here you can add a Target (same/new page) for Fourth Button.'];
$GLOBALS['TL_LANG']['tl_content']['dse_buttonFourText']  =
    ['Fourth Button Text', 'Here you can add a Text for Fourth Button.'];
$GLOBALS['TL_LANG']['tl_content']['dse_buttonFourColor']  =
    ['Background Color', 'Here you can add a Background Color for this button (use this as standalone option).'];

    $GLOBALS['TL_LANG']['tl_content']['margin_legend']   = 'Margin Settings';
$GLOBALS['TL_LANG']['tl_content']['dse_marginTop']   = ['Margin Top', 'Here you can add Margin to the top edge of the element (numbers only)'];
$GLOBALS['TL_LANG']['tl_content']['dse_marginBottom']   = ['Margin Bottom', 'Here you can add Margin to the bottom edge of the element (numbers only)'];