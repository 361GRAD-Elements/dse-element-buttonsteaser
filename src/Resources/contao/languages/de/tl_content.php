<?php

/**
 * 361GRAD Element Buttons Teaser
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

$GLOBALS['TL_LANG']['tl_content']['buttonsteaser_legend']   = 'Teaser Einstellungen';
$GLOBALS['TL_LANG']['CTE']['dse_elements'] = 'DSE-Elements';
$GLOBALS['TL_LANG']['CTE']['dse_buttonsteaser'] = ['Buttons Teaser', '1-4 Buttons Teaser'];

$GLOBALS['TL_LANG']['tl_content']['dse_bgcolor']  =
    ['Hintergrundfarbe', 'Hier können Sie eine Hintergrundfarbe für ganze Elemente hinzufügen.'];
$GLOBALS['TL_LANG']['tl_content']['dse_align']  =
    ['Buttons ausrichten', 'Hier können Sie eine Ausrichtung für die Schaltflächen definieren.'];

$GLOBALS['TL_LANG']['tl_content']['buttonone_legend']   = 'Erste Schaltflächeneinstellungen';
$GLOBALS['TL_LANG']['tl_content']['dse_buttonOneHref']  =
    ['Erster Button Link', 'Hier können Sie einen Link für den ersten Button hinzufügen.'];
$GLOBALS['TL_LANG']['tl_content']['dse_buttonOneTarget']  =
    ['Erster Button-Target', 'Hier kannst du ein Target (gleiche / neue Seite) für First Button hinzufügen.'];
$GLOBALS['TL_LANG']['tl_content']['dse_buttonOneText']  =
    ['Erster Button Text', 'Hier können Sie einen Text für den ersten Button hinzufügen.'];
$GLOBALS['TL_LANG']['tl_content']['dse_buttonOneColor']  =
    ['Hintergrundfarbe', 'Hier können Sie eine Hintergrundfarbe für diese Schaltfläche hinzufügen (verwenden Sie diese als Standalone-Option).'];

$GLOBALS['TL_LANG']['tl_content']['buttontwo_legend']   = 'Zweite Schaltflächeneinstellungen';
$GLOBALS['TL_LANG']['tl_content']['dse_buttonTwoHref']  =
    ['Zweite Button Link', 'Hier können Sie einen Link für den ersten Button hinzufügen.'];
$GLOBALS['TL_LANG']['tl_content']['dse_buttonTwoTarget']  =
    ['Zweite Button-Target', 'Hier kannst du ein Target (gleiche / neue Seite) für First Button hinzufügen.'];
$GLOBALS['TL_LANG']['tl_content']['dse_buttonTwoText']  =
    ['Zweite Button Text', 'Hier können Sie einen Text für den ersten Button hinzufügen.'];
$GLOBALS['TL_LANG']['tl_content']['dse_buttonTwoColor']  =
    ['Hintergrundfarbe', 'Hier können Sie eine Hintergrundfarbe für diese Schaltfläche hinzufügen (verwenden Sie diese als Standalone-Option).'];

$GLOBALS['TL_LANG']['tl_content']['buttonthree_legend']   = 'Dritte Schaltflächeneinstellungen';
$GLOBALS['TL_LANG']['tl_content']['dse_buttonThreeHref']  =
    ['Dritte Button Link', 'Hier können Sie einen Link für den ersten Button hinzufügen.'];
$GLOBALS['TL_LANG']['tl_content']['dse_buttonThreeTarget']  =
    ['Dritte Button-Target', 'Hier kannst du ein Target (gleiche / neue Seite) für First Button hinzufügen.'];
$GLOBALS['TL_LANG']['tl_content']['dse_buttonThreeText']  =
    ['Dritte Button Text', 'Hier können Sie einen Text für den ersten Button hinzufügen.'];
$GLOBALS['TL_LANG']['tl_content']['dse_buttonThreeColor']  =
    ['Hintergrundfarbe', 'Hier können Sie eine Hintergrundfarbe für diese Schaltfläche hinzufügen (verwenden Sie diese als Standalone-Option).'];

$GLOBALS['TL_LANG']['tl_content']['buttonfour_legend']   = 'Vierte Schaltflächeneinstellungen';
$GLOBALS['TL_LANG']['tl_content']['dse_buttonFourHref']  =
    ['Vierte Button Link', 'Hier können Sie einen Link für den ersten Button hinzufügen.'];
$GLOBALS['TL_LANG']['tl_content']['dse_buttonFourTarget']  =
    ['Vierte Button-Target', 'Hier kannst du ein Target (gleiche / neue Seite) für First Button hinzufügen.'];
$GLOBALS['TL_LANG']['tl_content']['dse_buttonFourText']  =
    ['Vierte Button Text', 'Hier können Sie einen Text für den ersten Button hinzufügen.'];
$GLOBALS['TL_LANG']['tl_content']['dse_buttonFourColor']  =
    ['Hintergrundfarbe', 'Hier können Sie eine Hintergrundfarbe für diese Schaltfläche hinzufügen (verwenden Sie diese als Standalone-Option).'];

$GLOBALS['TL_LANG']['tl_content']['margin_legend']   = 'Randeinstellungen';
$GLOBALS['TL_LANG']['tl_content']['dse_marginTop']   = ['Rand oben', 'Hier können Sie Margin zum oberen Rand des Elements hinzufügen (nur nummern)'];
$GLOBALS['TL_LANG']['tl_content']['dse_marginBottom']   = ['Rand unten', 'Hier können Sie dem unteren Rand des Elements Rand hinzufügen (nur nummern)'];    