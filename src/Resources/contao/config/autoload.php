<?php

/**
 * 361GRAD Element Buttons Teaser
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

// Templates
TemplateLoader::addFiles(
    [
        'ce_dse_buttonsteaser'      => 'vendor/361grad-elements/dse-element-buttonsteaser/src/Resources/contao/templates'
    ]
);
