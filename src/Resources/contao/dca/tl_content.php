<?php

/**
 * 361GRAD Element Buttons Teaser
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

// Element palettes
$GLOBALS['TL_DCA']['tl_content']['palettes']['dse_buttonsteaser'] =
    '{type_legend},type;' .
    '{buttonsteaser_legend},dse_bgcolor,dse_align;' .
    '{text_legend},headline;' .
    '{buttonone_legend},dse_buttonOneHref,dse_buttonOneText,dse_buttonOneTarget,dse_buttonOneColor;' .
    '{buttontwo_legend},dse_buttonTwoHref,dse_buttonTwoText,dse_buttonTwoTarget,dse_buttonTwoColor;' .
    '{buttonthree_legend},dse_buttonThreeHref,dse_buttonThreeText,dse_buttonThreeTarget,dse_buttonThreeColor;' .
    '{buttonfour_legend},dse_buttonFourHref,dse_buttonFourText,dse_buttonFourTarget,dse_buttonFourColor;' .
    '{margin_legend},dse_marginTop,dse_marginBottom;' .
    '{invisible_legend:hide},invisible,start,stop';

// Element fields
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_bgcolor'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_bgcolor'],
    'default'   => 'none',
    'inputType' => 'radio',
    'options'   => [
        'none',
        'bg-black',
        'bg-middlegrey',
        'bg-lightgrey',
        'bg-ultralightgrey',
        'bg-white'
    ],
    'reference' => [
        'none'              => Dse\ElementsBundle\ElementImageText\Element\ContentDseImageText::refColor(
            '#FFF',
            '#000',
            'Keine'
        ),
        'bg-black'          => Dse\ElementsBundle\ElementImageText\Element\ContentDseImageText::refColor(
            '#141312',
            '#FFF',
            'Black'
        ),
        'bg-middlegrey'     => Dse\ElementsBundle\ElementImageText\Element\ContentDseImageText::refColor(
            '#8D8986',
            '#000',
            'MiddleGrey'
        ),
        'bg-lightgrey'      => Dse\ElementsBundle\ElementImageText\Element\ContentDseImageText::refColor(
            '#C6C4C2',
            '#000',
            'LightGrey'
        ),
        'bg-ultralightgrey' => Dse\ElementsBundle\ElementImageText\Element\ContentDseImageText::refColor(
            '#E3E2E2',
            '#000',
            'UltralightGrey'
        ),
        'bg-white'          => Dse\ElementsBundle\ElementImageText\Element\ContentDseImageText::refColor(
            '#F7F7F7',
            '#000',
            'NaturalWhite'
        )
    ],

    'eval'      => [
        'tl_class' => 'w100',
    ],
    'sql'       => "varchar(32) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_align'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_align'],
    'default'   => 'center',
    'inputType' => 'radio',
    'options'   => [
        'left',
        'center',
        'right'
    ],
    'eval'      => [
        'tl_class' => 'w100',
    ],
    'sql'       => "varchar(32) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_buttonOneHref'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_buttonOneHref'],
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50 wizard'
    ],
    'wizard'    => [
        [
            'tl_content',
            'pagePicker',
        ]
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_buttonTwoHref'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_buttonTwoHref'],
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50 wizard'
    ],
    'wizard'    => [
        [
            'tl_content',
            'pagePicker',
        ]
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_buttonThreeHref'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_buttonThreeHref'],
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50 wizard'
    ],
    'wizard'    => [
        [
            'tl_content',
            'pagePicker',
        ]
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_buttonFourHref'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_buttonFourHref'],
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50 wizard'
    ],
    'wizard'    => [
        [
            'tl_content',
            'pagePicker',
        ]
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_buttonOneText'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_buttonOneText'],
    'inputType' => 'text',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w50',
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_buttonTwoText'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_buttonTwoText'],
    'inputType' => 'text',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w50',
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_buttonThreeText'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_buttonThreeText'],
    'inputType' => 'text',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w50',
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_buttonFourText'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_buttonFourText'],
    'inputType' => 'text',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w50',
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_buttonOneTarget'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_buttonOneTarget'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'clr w50 m12',
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_buttonTwoTarget'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_buttonTwoTarget'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'clr w50 m12',
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_buttonThreeTarget'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_buttonThreeTarget'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'clr w50 m12',
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_buttonFourTarget'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_buttonFourTarget'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'clr w50 m12',
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_buttonOneColor'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_buttonOneColor'],
    'default'   => 'none',
    'inputType' => 'radio',
    'options'   => [
        // #000 black
        'black',
        // #FFF weiß
        'white',
        // #cfcfcf grey
        'grey',
        'none'
    ],
    'reference' => [
        'black' => Dse\ElementsBundle\ElementButtonsTeaser\Element\ContentDseButtonsTeaser::refColor(
            '#000',
            '#FFF',
            'Black'
        ),
        'white'      => Dse\ElementsBundle\ElementButtonsTeaser\Element\ContentDseButtonsTeaser::refColor(
            '#FFF',
            '#000',
            'Weiß'
        ),
        'grey'     => Dse\ElementsBundle\ElementButtonsTeaser\Element\ContentDseButtonsTeaser::refColor(
            '#cfcfcf',
            '#000',
            'Grey'
        ),
    ],
    'eval'      => [
        'tl_class' => 'clr w100',
    ],
    'sql'       => "varchar(32) NOT NULL default ''"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_buttonTwoColor'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_buttonTwoColor'],
    'default'   => 'none',
    'inputType' => 'radio',
    'options'   => [
        // #000 black
        'black',
        // #FFF weiß
        'white',
        // #cfcfcf grey
        'grey',
        'none'
    ],
    'reference' => [
        'black' => Dse\ElementsBundle\ElementButtonsTeaser\Element\ContentDseButtonsTeaser::refColor(
            '#000',
            '#FFF',
            'Black'
        ),
        'white'      => Dse\ElementsBundle\ElementButtonsTeaser\Element\ContentDseButtonsTeaser::refColor(
            '#FFF',
            '#000',
            'Weiß'
        ),
        'grey'     => Dse\ElementsBundle\ElementButtonsTeaser\Element\ContentDseButtonsTeaser::refColor(
            '#cfcfcf',
            '#000',
            'Grey'
        ),
    ],
    'eval'      => [
        'tl_class' => 'clr w100',
    ],
    'sql'       => "varchar(32) NOT NULL default ''"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_buttonThreeColor'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_buttonThreeColor'],
    'default'   => 'none',
    'inputType' => 'radio',
    'options'   => [
        // #000 black
        'black',
        // #FFF weiß
        'white',
        // #cfcfcf grey
        'grey',
        'none'
    ],
    'reference' => [
        'black' => Dse\ElementsBundle\ElementButtonsTeaser\Element\ContentDseButtonsTeaser::refColor(
            '#000',
            '#FFF',
            'Black'
        ),
        'white'      => Dse\ElementsBundle\ElementButtonsTeaser\Element\ContentDseButtonsTeaser::refColor(
            '#FFF',
            '#000',
            'Weiß'
        ),
        'grey'     => Dse\ElementsBundle\ElementButtonsTeaser\Element\ContentDseButtonsTeaser::refColor(
            '#cfcfcf',
            '#000',
            'Grey'
        ),
    ],
    'eval'      => [
        'tl_class' => 'clr w100',
    ],
    'sql'       => "varchar(32) NOT NULL default ''"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_buttonFourColor'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_buttonFourColor'],
    'default'   => 'none',
    'inputType' => 'radio',
    'options'   => [
        // #000 black
        'black',
        // #FFF weiß
        'white',
        // #cfcfcf grey
        'grey',
        'none'
    ],
    'reference' => [
        'black' => Dse\ElementsBundle\ElementButtonsTeaser\Element\ContentDseButtonsTeaser::refColor(
            '#000',
            '#FFF',
            'Black'
        ),
        'white'      => Dse\ElementsBundle\ElementButtonsTeaser\Element\ContentDseButtonsTeaser::refColor(
            '#FFF',
            '#000',
            'Weiß'
        ),
        'grey'     => Dse\ElementsBundle\ElementButtonsTeaser\Element\ContentDseButtonsTeaser::refColor(
            '#cfcfcf',
            '#000',
            'Grey'
        ),
    ],
    'eval'      => [
        'tl_class' => 'clr w100',
    ],
    'sql'       => "varchar(32) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_marginTop'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_marginTop'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => "text NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_marginBottom'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_marginBottom'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => "text NULL"
];